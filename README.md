# ImdbPoster

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'imdb_poster'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install imdb_poster

## Usage

    require "imdb_poster"
    ImdbPoster.download("Iron Man", "movie-posters/") # Saves image to a given path.

### OR

    require "imdb_poster"
    ImdbPoster.download("Iron Man") # Saves image to current directory.