require "imdb_poster/version"
require 'open-uri'
require "fileutils"
require "nokogiri"
require "gss"

module ImdbPoster

    # This will save the movie poster you chose, to the path you chose.
    # It is that simple.
    #
    # @param movie_title [String] the name of the movie.
    # @param path [String] optional. The path to save the poster too.
    def self.download(movie_title, path="")
        (top_result, results) = Gss.return_results("http://imdb.com", movie_title)

        noko = Nokogiri::HTML(open(top_result[:url]))

        video_title = noko.css("div.title-overview h1 > span")[0].text.downcase.gsub(" ", "-")
        img_url = noko.css("div.title-overview td#img_primary img")[0]["src"]
        img_ext = img_url[-3..-1]

        FileUtils::mkdir_p "#{path}"
        open("#{path}#{video_title}-poster.#{img_ext}", 'wb') do |file|
            file << open(img_url).read
        end
    end
end
