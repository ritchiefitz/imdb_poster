# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'imdb_poster/version'

Gem::Specification.new do |spec|
  spec.name          = "imdb_poster"
  spec.version       = ImdbPoster::VERSION
  spec.authors       = ["rfitzger"]
  spec.email         = ["ritchiefitz1@gmail"]
  spec.summary       = %q{Download movie posters from IMDB.}
  spec.description   = %q{Download movie posters from IMDB. Just give the name of the movie, and the path to where you want to save it. It's that easy.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.6"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency "nokogiri"
  spec.add_runtime_dependency "gss"
end
